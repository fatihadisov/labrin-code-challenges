import React, { useState, useRef, useEffect } from 'react';
import { InputGroup, FormControl, Button, Col } from 'react-bootstrap';
import { connect } from "react-redux";
import { fetchSearchCapital } from '../../store/SearchCapitals/actions.js';

const mapStateToProps = (state) => ({
  capitals: state.searchCapital.searchedCapitals,
})

export const SearchInput = connect(mapStateToProps, { fetchSearchCapital })(
  ({
    fetchSearchCapital,
    capitals
  }) => {

    const [inputVal, setInputVal] = useState("");
    const [capitalsInList, setCapitalsInList] = useState([]);
    const [showCapitalExistSign, setShowCapitalExistSign] = useState('opacity-0');
    const ref = useRef(null);

    useEffect(() => {
      ref.current.focus();
    }, [ref])

    useEffect(() => {
      let capitalArr = []
      capitals.forEach(({ capitals }) => {
        capitalArr.push(capitals.toLowerCase());
      });
      !capitalsInList.includes(capitals) && setCapitalsInList([...capitalArr])
    }, [capitals])

    const onSearch = () => {
      searchCapital();
    }

    const onPressEnter = (e) => {
      const key = e.keyCode
      if (key === 'Enter' || key === 13) {
        searchCapital();
      }
    }

    const isCapitalExist = (capital) => capitalsInList.includes(capital.toLowerCase())

    const searchCapital = () => {
      const input = document.querySelector('.input-search')

      if (isCapitalExist(inputVal)) {
        input.classList.add('animated', 'shake');
        setShowCapitalExistSign('');
        setTimeout(() => {
          input.classList.remove('animated', 'shake');
        }, 500);
      } else {

        inputVal && fetchSearchCapital(inputVal)
        setShowCapitalExistSign('opacity-0');
      }
      setInputVal('');
    }


    return (
      <>
        <Col xl={10} lg={10} md={10} sm={12} xs={12}>
          <InputGroup className="mb-1">
            <FormControl
              placeholder="Capital"
              aria-label="Capital"
              aria-describedby="basic-addon2"
              className="input-search"
              value={inputVal}
              onChange={(e) => setInputVal(e.target.value)}
              onKeyUp={e => onPressEnter(e)}
              ref={ref}
            />
          </InputGroup>
          <span className={`m-0 pl-2 text-danger ${showCapitalExistSign}`}>Already exist!</span>
        </Col>
        <Col xl={2} lg={2} md={2} sm={12} xs={12}>
          <Button className="w-100" onClick={onSearch} variant="primary">Submit</Button>
        </Col>
      </>
    )
  })
