import React, { useEffect, useState } from 'react'
import { ListGroup, Button } from 'react-bootstrap';
import { connect } from "react-redux";
import { removeSelectedCapital } from '../../store/SearchCapitals/actions';
import { fetchCapitalDetail } from '../../store/CapitalDetail/actions';
import { Transition, animated } from 'react-spring/renderprops'


const mapStateToProps = (state) => ({
  capitals: state.searchCapital.searchedCapitals,
  isLoading: state.searchCapital.loading,
  capitalData: state.searchCapital
})

export const ChosenCapitals = connect(mapStateToProps, { removeSelectedCapital, fetchCapitalDetail })(
  ({ capitals, isLoading, capitalData, removeSelectedCapital, fetchCapitalDetail }) => {


    const onClickRemove = (e) => {
      const item = e.target.attributes.getNamedItem("data-remove").value;
      removeSelectedCapital(item);
    }

    const onClickCapital = (e) => {
      const item = e.target
      const itemClassNames = item.attributes.getNamedItem('class').value
      const listItems = document.querySelectorAll('.list-item');
      
      if (itemClassNames.includes('list-item')) {
        listItems.forEach((item) => item.classList.remove('active'));
        item.classList.add('active');

        const alpha = item.children[0].attributes.getNamedItem('data-alpha').value
        fetchCapitalDetail(alpha);
      }
    }

    return (
      <ListGroup defaultActiveKey="#link0">
        <Transition
          native
          items={true}
          from={{ overflow: 'hidden', height: 0, borderRadius: 'inherit' }}
          enter={[{ height: 'auto' }]}
          leave={{ height: 0 }}
        >
          {show =>
            show && (props => <animated.div className="list-item-container" style={props}>
              {isLoading &&
                <ListGroup.Item className="d-flex justify-content-between" key={'-1'} action >
                  <div className="on-load"><div className="spinner"></div></div>
                  <Button variant="danger" className="cancel-button p-0 px-1">&#10005;</Button>
                </ListGroup.Item>}
              {capitals.slice(0).reverse().map((capital, i) => {
                const { capitals, alpha } = capital
                return (
                  capitals.length
                    ? <ListGroup.Item
                      className={`d-flex list-item justify-content-between align-items-baseline ${i === 0 && 'active'}`}
                      key={i}
                      action
                      onClick={e => onClickCapital(e)}
                    >
                      {capitals}
                      <span
                        onClick={(e) => onClickRemove(e)}
                        data-alpha={alpha}
                        data-remove={capitals}
                        variant="danger"
                        className="cancel-button btn p-0 px-1"
                      >
                        &#10005;
                      </span>
                    </ListGroup.Item>
                    : ''
                )
              })
              }
            </animated.div>)
          }
        </Transition>
      </ListGroup>
    )
  })
