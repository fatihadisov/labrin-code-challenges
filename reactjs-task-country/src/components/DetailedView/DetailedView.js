import React, { useEffect, useState } from 'react'
import { Card } from 'react-bootstrap'
import { connect } from 'react-redux';
import { fetchBorders } from '../../store/CapitalDetail/actions';

const mapStateToProps = (state) => ({
  capitalDetail: state.capitalDetail,
  capitals: state.searchCapital.searchedCapitals,
  borderArr: state.capitalDetail.borders,
  isLoading: state.capitalDetail.loading,
})

export const DetailedView = connect(mapStateToProps, { fetchBorders })
  (({
    capitalDetail,
    isLoading,
    fetchBorders,
    capitals,
    borderArr
  }) => {

    const countryDetail = capitalDetail.res

    const {
      flag,
      nativeName,
      region,
      population,
      currencies,
      borders
    } = countryDetail

    const isObjEmpty = obj => Object.keys(obj).length > 0 ? false : true

    useEffect(() => {
      borders && borders.map((alphas) => fetchBorders(alphas))
    }, [borders])

    return (
      isLoading
        ? <Card className="text-left">
          <Card.Header className="load-animate detail-flag"></Card.Header>
          <Card.Body>
            <Card.Title className="load-animate detail-title"></Card.Title>
            <Card.Text className="text-muted load-animate detail-region">
            </Card.Text>
            <Card.Text className="load-animate detail-population">
            </Card.Text >
            <Card.Text className="load-animate detail-currency">
            </Card.Text>
          </Card.Body>
          <Card.Footer className="text-muted">
            <Card.Text className="load-animate detail-border">
            </Card.Text>
          </Card.Footer>
        </Card>
        : !isObjEmpty(countryDetail)
          ? <Card className="text-left">
            <Card.Header>
              <img className="flag-img" src={flag} />
            </Card.Header>
            <Card.Body>
              <Card.Title>{nativeName}</Card.Title>
              <Card.Text className="text-muted">
                {region}
              </Card.Text>
              <Card.Text>
                Populations: {population}
              </Card.Text>
              <Card.Text>
                Currency: {currencies && currencies.map((currency) => currency.code)}
              </Card.Text>
            </Card.Body>
            <Card.Footer className='text-muted'>
              <Card.Text className={`${!borderArr.length && 'load-animate detail-border'}`}>
                {borderArr.length ? 'Neighbours: ' : ''}
                {borderArr && borderArr.map((border, i) => {
                  return borderArr.length - i === 1
                    ? border.name
                    : border.name + ", "
                })}
              </Card.Text>
            </Card.Footer>
          </Card>
          : ""
    )
  })
