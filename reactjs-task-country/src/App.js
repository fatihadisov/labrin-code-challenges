import React from 'react';
import { SearchInput, ChosenCapitals, DetailedView } from './components';
import { Container, Row, Col } from 'react-bootstrap'


function App() {
  return (
    <Container>
      <Row className="mt-5">
        <SearchInput />
      </Row>

      <Row className="mt-5">
        <Col xl={6} lg={6} md={6} sm={12} xs={12} className="mb-5">
          <ChosenCapitals />
        </Col>
        <Col xl={6} lg={6} md={6} sm={12} xs={12}>
          <DetailedView />
        </Col>
      </Row>
    </Container>
  );
}

export default App;
