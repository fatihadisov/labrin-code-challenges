import { combineReducers } from 'redux';
import { capitalDetailReducer } from './CapitalDetail/reducers';
import { capitalSearchReducer } from './SearchCapitals/reducers';


export default combineReducers({
  searchCapital: capitalSearchReducer,
  capitalDetail: capitalDetailReducer
})