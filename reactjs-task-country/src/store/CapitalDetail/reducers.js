import { SUCCESS_DETAIL, ERROR_DETAIL, CLEAR_DETAIL, FETCH_BORDERS } from './constants';


let initialStore = {
  loading: false,
  error: [],
  res: {},
  borders: []
}

export const capitalDetailReducer = (state = initialStore, action) => {
  switch (action.type) {
    case SUCCESS_DETAIL:
      return {
        ...state,
        loading: false,
        res: action.data
      }
    case ERROR_DETAIL:
      return {
        ...state,
        loading: false,
        error: action.error
      }
    case FETCH_BORDERS:
      return {
        ...state,
        borders: [...state.borders, action.data]
      }
    case CLEAR_DETAIL:
      return {
        loading: true,
        error: [],
        res: {},
        borders: []
      }

    default:
      return state
  }
}