import axios from 'axios';
import { SUCCESS_DETAIL, ERROR_DETAIL, CLEAR_DETAIL, FETCH_BORDERS } from "./constants";
import api from "../../config/config";

const fetchApi = api.capitalDetailApiUrl;

export const fetchCapitalDetail = (val) => {
  console.log(val)
  return (dispatch) => {
    axios
      .get(`${fetchApi}${val}`)
      .then((res) => {
        dispatch({ type: CLEAR_DETAIL })
        setTimeout(() => {
          dispatch({
            type: SUCCESS_DETAIL,
            data: res.data,
          });
        }, 1000);
      })
      .catch((err) => {
        dispatch({
          type: ERROR_DETAIL,
          error: err,
        });
      });
  }
}

export const fetchBorders = val => {
  return (dispatch) => {
    axios
      .get(`${fetchApi}${val}`)
      .then((res) => {
        // dispatch({ type: CLEAR_DETAIL })
        setTimeout(() => {
          dispatch({
            type: FETCH_BORDERS,
            data: res.data,
          });
        }, 1000);
      })
      .catch((err) => {
        dispatch({
          type: ERROR_DETAIL,
          error: err,
        });
      });
  }
}