import axios from 'axios';
import {
  SUCCESS_SEARCH,
  ERROR_SEARCH,
  CLEAR_SEARCH,
  REMOVE_SELECTED_SEARCH
} from "./constants";
import api from "../../config/config";

const fetchApi = api.capitalSearchApiUrl;

export const fetchSearchCapital = (val) => {
  return (dispatch) => {
    axios
      .get(`${fetchApi}${val}`)
      .then((res) => {
        dispatch({
          type: CLEAR_SEARCH
        })
        setTimeout(() => {
          dispatch({
            type: SUCCESS_SEARCH,
            data: res.data,
          });
        }, 1000);
      })
      .catch((err) => {
        dispatch({
          type: ERROR_SEARCH,
          error: err,
        });
      });
  }
}

export const clearCapitalSearchResult = () => {
  return (dispatch) => {
    dispatch({
      type: CLEAR_SEARCH
    })
  }
}


export const removeSelectedCapital = (capital) => {
  return (dispatch) => {
    dispatch({
      type: REMOVE_SELECTED_SEARCH,
      data: capital
    })
  }
}