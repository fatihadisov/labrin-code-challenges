import { 
  SUCCESS_SEARCH, 
  ERROR_SEARCH, 
  CLEAR_SEARCH, 
  REMOVE_SELECTED_SEARCH 
} from './constants';


let initialStore = {
  loading: false,
  error: {},
  res: [],
  searchedCapitals: []
}

export const capitalSearchReducer = (state = initialStore, action) => {
  switch (action.type) {
    case SUCCESS_SEARCH:
      return {
        ...state,
        loading: false,
        res: action.data,
        searchedCapitals: [...state.searchedCapitals, {
          alpha: action.data[0].alpha2Code,
          capitals: action.data[0].capital
        }]
      }
    case ERROR_SEARCH:
      return {
        ...state,
        loading: false,
        error: action.data,
        searchedCapitals: [...state.searchedCapitals, {
          alpha: action.data[0].alpha2Code,
          capitals: action.data[0].capital
        }]
      }
    case REMOVE_SELECTED_SEARCH:
      const newArr = state.searchedCapitals.filter(({ capitals }) => {
        return capitals !== action.data
      })
      return {
        loading: false,
        error: {},
        res: [],
        searchedCapitals: newArr
      }
    case CLEAR_SEARCH:
      return {
        loading: true,
        error: {},
        res: [],
        searchedCapitals: state.searchedCapitals
      }
    default:
      return state
  }
}