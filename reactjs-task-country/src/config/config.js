let config = {
  capitalSearchApiUrl: process.env.REACT_APP_CAPITAL_SEARCH_API,
  capitalDetailApiUrl: process.env.REACT_APP_CAPITAL_DETAIL_API,
}

export default config